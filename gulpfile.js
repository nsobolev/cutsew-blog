const gulp = require('gulp'),
      less = require('gulp-less'),
      glob = require('gulp-less-glob'),
      plumber = require('gulp-plumber'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      postcssflexbugsfixes = require('postcss-flexbugs-fixes'),
      sourcemaps = require('gulp-sourcemaps'),
      browserSync = require('browser-sync').create();

const paths = {
    styles: {
      src: 'src/styles/style.less',
      dest: 'dist/css/'
    }
  };

gulp.task('less-style', () => {
    gulp.src(paths.styles.src)
      .pipe(plumber())
      .pipe(sourcemaps.init())
      .pipe(glob())
      .pipe(less())
      .pipe(postcss([
        autoprefixer(),
        postcssflexbugsfixes()
      ]))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(paths.styles.dest))
      .pipe(browserSync.stream()); // Update browser-sync
});

gulp.task('browser-sync', gulp.series('less-style', (done) => {
    browserSync.init({
      server: 'dist/'
    });

    gulp.watch('src/styles/*.less', gulp.series('less-style'));
    gulp.watch('src/*.html').on('change', server.reload);
    done();
}));